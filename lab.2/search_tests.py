import pytest
from search_lib import linear_search, binary_search, naive_search, kmp_search
elem_1 = 1
elem_s = 'stt'
lst_num =[2, 2, 3, 4, 1, 6, 4 ,5 ,6 ,0]
lst_str = 'stststtest'
lst_input = [1, 2, 3, 'sdsdsdsd']


def test_search_subtxt():
    assert all([4 == linear_search(lst_num , elem_1),
                4 == binary_search(lst_num, elem_1),
                4 == naive_search(lst_str, elem_s),
                4 == kmp_search(lst_str, elem_s)])


def test_input_data():
    try:
        all([4 == linear_search(lst_input, elem_1),
             4 == binary_search(lst_input, elem_1),
             4 == naive_search(lst_input, elem_s),
             4 == kmp_search(lst_input, elem_s)])
    except IndexError:
        assert False
    else:
        assert True
