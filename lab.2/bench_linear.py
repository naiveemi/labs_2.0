import cProfile
import pstats
from pstats import SortKey
import matplotlib.pyplot as plt
from random import uniform
from search_lib import linear_search


def n_complexity(arr):
    return [e for e in arr]


def correct_lines_c(f):
    result = 1
    lines = [line.strip() for line in f]
    lines_1 = [line for line in lines if line not in '' and line.split()[0].isnumeric() and not line.count('function')]
    for line in lines_1:
        if line.find('swap') != -1:
            result = int(line.split('   ')[0])
            break
    return result


operation_count = []
operation_lst = []
step = 1000
max_size = 10000
min_size = 1000
arr = [i for i in range(min_size, max_size, step)]

for j in range(min_size, max_size, step):
    lst = []
    for x in range(j):
        lst.append(uniform(-100, 100))
    elem = lst[-10]
    operation_count.append(len(lst))
    cProfile.run('linear_search(lst, elem)', 'stats.log')
    with open('output.txt', 'w') as log_file_stream:
        p = pstats.Stats('stats.log', stream=log_file_stream)
        p.strip_dirs().sort_stats(SortKey.CALLS).print_stats()
    f = open('output.txt')
    line = correct_lines_c(f)
    f.close()
    operation_lst.append(int(line))
operation_lst_c = [i for i in operation_lst]

plt.style.use('seaborn-whitegrid')
fig, ax = plt.subplots()

ax.set_title('Сложность (линейный поиск)', fontsize=10, c='indigo')
ax.set_xlabel('Размер массива', fontsize=10, c='indigo')
ax.set_ylabel('Количество операций', fontsize=10, c='indigo')
ax.plot(operation_count, n_complexity(arr), lw=2, label='Теоретическая сложность')
ax.plot(operation_count, operation_lst_c, lw=2, c='g', linestyle='--', label='Практическая сложность')
#ax.plot(operation_count, operation_lst_c, lw=2, c='g', label='linear_search')

plt.legend()
plt.show()
