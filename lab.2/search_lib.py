def swap():
    return None


def linear_search(lst, elem):
    for i in range(len(lst)):
        swap()
        if lst[i] == elem:
            return i
    return -1


def binary_search(lst, elem):
    first = 0
    last = len(lst) - 1
    index = -1
    while (first <= last) and (index == -1):
        swap()
        mid = (first + last) // 2
        if lst[mid] == elem:
            index = mid
        else:
            if elem < lst[mid]:
                last = mid - 1
            else:
                first = mid + 1
    return index


def naive_search(txt, Subtxt):
    len_subtxt = len(Subtxt)
    len_txt = len(txt)

    for i in range(len_txt - len_subtxt + 1):
        status = 1
        for j in range(len_subtxt):
            swap()
            if txt[i + j] != Subtxt[j]:
                status = 0
                break
        if j == len_subtxt - 1 and status != 0:
            return i


def prefix(s):
    v = [0] * len(s)
    for i in range(1, len(s)):
        k = v[i - 1]
        while k > 0 and s[k] != s[i]:
            k = v[k - 1]
        if s[k] == s[i]:
            k = k + 1
        v[i] = k
    return v


def kmp_search(txt, Subtxt):
    index = -1
    f = prefix(Subtxt)
    k = 0
    for i in range(len(txt)):
        swap()
        while k > 0 and (Subtxt[k] != txt[i]):
            k = f[k - 1]
        if Subtxt[k] == txt[i]:
            k = k + 1
        if k == len(Subtxt):
            index = i - len(Subtxt) + 1
            break
    return index
