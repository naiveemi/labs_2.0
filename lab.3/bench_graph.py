import networkx as nx
import cProfile
import pstats
from graph_lib import get_my_graph, find_all_paths
from matplotlib import pyplot as plt

def line_with_calls(file):
    result = 1
    lines = [i.strip() for i in file]
    lines_1 = [i for i in lines if i not in '' and i.split()[0].isnumeric() and not i.count('function')]
    for line in lines_1:
        if line.find('swap') != -1:
            result = line.split('   ')[0]
            break
    return result

min_size = 1
max_size = 10
step = 1
len_arr = [i for i in range(min_size, max_size, step)]
operations = []

for i in range(min_size, max_size, step):
    G = nx.complete_graph(i)
    graph = get_my_graph(list(G.nodes()), list(G.edges()))
    cProfile.run('find_all_paths(graph, 0, i)', 'stats.log')
    with open('output.txt', 'w') as log_file:
        p = pstats.Stats('stats.log', stream=log_file)
        p.strip_dirs().sort_stats(pstats.SortKey.CALLS).print_stats()
    f = open('output.txt')
    line = line_with_calls(f)
    f.close()
    operations.append(int(line))
    print(i)

plt.style.use('seaborn-whitegrid')
fig, ax = plt.subplots()

ax.set_title('Cложность алгоритма поиска кратчайшего пути в неориентированном графе', c='indigo', fontsize=10)
ax.set_xlabel('Размер массива', fontsize=10, c='indigo')
ax.set_ylabel('Количество операций', fontsize=10, c='indigo')
ax.plot(len_arr, operations, lw=2, c='m', label='Практическая сложность')
plt.legend()
plt.show()
