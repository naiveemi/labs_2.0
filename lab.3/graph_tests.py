import networkx as nx
from graph_lib import graph_loader, get_nx_graph, get_my_graph, find_all_paths


def test_shortest_path():
    # load data
    graph_data = graph_loader('data.txt')

    # create graph
    nx_graph = get_nx_graph(graph_data)
    my_graph = get_my_graph(graph_data[0], graph_data[1])

    # get shortest_path
    expected_path = nx.shortest_path(nx_graph, source='A', target='H')
    my_path = min([(len(e), e) for e in find_all_paths(my_graph, 'A', 'H')])[1]

    assert my_path == expected_path

