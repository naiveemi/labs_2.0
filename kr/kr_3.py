from random import random
import numpy as np
import matplotlib.pyplot as plt

size = list(np.linspace(10, 100000, 1000))
pi = []
pi_line = [3.14 for i in range(len(size))]


def mc_multiple_runs(size, hits=0):
    for i in range(int(size)):
        x, y = random(), random()
        if x ** 2 + y ** 2 < 1:
            hits = hits + 1
    return float(hits)


for i in size:
    pi.append(4 * (mc_multiple_runs(i) / i))
    print('hits : %d, trials: %d, estimate pi = %1.4F' % (mc_multiple_runs(i), i, 4 * (mc_multiple_runs(i) / i)))

fig, ax = plt.subplots()

plt.style.use('seaborn-whitegrid')
ax.plot(size, pi, 'blue')
ax.plot(size, pi_line, 'red')
plt.title('График изменения значения числа Пи методом Монте-Карло')
plt.xlabel('Число испытаний')
plt.ylabel('Расчетное значение числа Пи')
plt.ylim(3.11, 3.17)
plt.show()

