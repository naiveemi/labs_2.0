import math


def rectangle_method(func, a, b, nseg):
    dx = 1.0 * (b - a) / nseg
    sum = 0.0
    x_start = a + dx
    for i in range(nseg):
        sum += func(x_start + i * dx)

    return sum * dx


def trapezoid_method(func, a, b, nseg):
    dx = 1.0 * (b - a) / nseg
    sum = 0.5 * (func(a) + func(b))
    for i in range(1, nseg):
        sum += func(a + i * dx)

    return sum * dx


def simpson_method(f, a, b, n):
    h = (b - a) / n
    k = 0.0
    x = a + h
    for i in range(1, n // 2 + 1):
        k += 4 * f(x)
        x += 2 * h
    x = a + 2 * h
    for i in range(1, n // 2):
        k += 2 * f(x)
        x += 2 * h
    return (h / 3) * (f(a) + f(b) + k)


a = 10
x = 10


def g(x):
    return 10 * (-45 * a ** 2 + 49 * a * x + 6 * x ** 2) / (15 * a ** 2 + 49 * a * x + 24 * x ** 2)


print('Метод прямоугольников:', rectangle_method(g, 1, 10, 5))
print('Метод трапеций:', trapezoid_method(g, 1, 10, 5))
print('Метод Симпсона:', simpson_method(g, 1, 10, 5))
