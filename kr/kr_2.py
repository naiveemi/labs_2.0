import kr_1
import matplotlib.pyplot as plt

a = 0.1
x = 0.2

integrated_values = ([], [], [], [], [])
inter = []


def g(x):
    return 10*(-45*a**2+49*a*x+6*x**2)/(15*a**2+49*a*x+24*x**2)


for i in range(10, 100, 10):
    integrated_values[0].append(kr_1.rectangle_method(g, 1, 10, i))
    integrated_values[1].append(kr_1.trapezoid_method(g, 1, 10, i))
    integrated_values[2].append(kr_1.simpson_method(g, 1, 10, i))
    inter.append(i)

for i in range(3):
    for j in range(len(integrated_values[i])):
        integrated_values[i][j] = (-0.25 - integrated_values[i][j])


plt.style.use('seaborn-whitegrid')
fig, ax = plt.subplots()

ax.set_title('Графики изменения разности', c='indigo', fontsize=10)
ax.plot(inter, integrated_values[0], lw=2, c='r', label="Метод прямоугольников")
ax.plot(inter, integrated_values[1], lw=2,c ='g', label="Метод трапеций")
ax.plot(inter, integrated_values[2], lw=2,c='m', label="Метод Симпосона")
ax.set_xlabel("Количество точек разбиения", c='indigo')
ax.set_ylabel("Истинное значение", c='indigo')
plt.legend()
plt.show()
