import pytest
from random import randint
from sort_lib import bubble_sort, shell_sort, insertion_sort, quicksort


def test_ascending_bubble_sort():
    lst = []
    for i in range(20):
        lst.append(randint(-100, 100))
    theory = sorted(lst)
    practice = bubble_sort(lst)
    assert theory == list(practice)


def test_descending_bubble_sort():
    lst = []
    for i in range(20):
        lst.append(randint(-100, 100))
    theory = list(reversed(sorted(lst)))
    practice = bubble_sort(lst, reverse=True)
    assert theory == list(practice)


def test_input_data_bubble_sort():
    test_lst = [1, 2, 3.41125, '3.14', 'abc']
    with pytest.raises(TypeError):
        list(bubble_sort(test_lst))


def test_stability_ascending_bubble_sort():
    lst = [(1, 1), (2, 5), (3, 2), (4, 6), (5, 5), (6, 10), (7, 8), (8, 5)]
    theory = sorted(lst)
    practice = shell_sort(lst)
    assert theory == list(practice)


def test_stability_descending_bubble_sort():
    lst = [(1, 1), (2, 5), (3, 2), (4, 6), (5, 5), (6, 10), (7, 8), (8, 5)]
    theory = list(reversed(sorted(lst)))
    practice = shell_sort(lst, reverse=True)
    assert theory == list(practice)


def test_ascending_shell_sort():
    lst = []
    for i in range(20):
        lst.append(randint(-100, 100))
    theory = sorted(lst)
    practice = shell_sort(lst)
    assert theory == list(practice)


def test_descending_shell_sort():
    lst = []
    for i in range(20):
        lst.append(randint(-100, 100))
    theory = list(reversed(sorted(lst)))
    practice = shell_sort(lst, reverse=True)
    assert theory == list(practice)


def test_input_data_shell_sort():
    test_lst = [1, 2, 3.41125, '3.14', 'abc']
    with pytest.raises(TypeError):
        list(shell_sort(test_lst))


def test_stability_ascending_shell_sort():
    lst = [(1, 1), (2, 5), (3, 2), (4, 6), (5, 5), (6, 10), (7, 8), (8, 5)]
    theory = sorted(lst)
    practice = shell_sort(lst)
    assert theory == list(practice)


def test_stability_descending_shell_sort():
    lst = [(1, 1), (2, 5), (3, 2), (4, 6), (5, 5), (6, 10), (7, 8), (8, 5)]
    theory = list(reversed(sorted(lst)))
    practice = shell_sort(lst, reverse=True)
    assert theory == list(practice)


def test_ascending_insertion_sort():
    lst = []
    for i in range(20):
        lst.append(randint(-100, 100))
    theory = sorted(lst)
    practice = insertion_sort(lst)
    assert theory == list(practice)


def test_descending_insertion_sort():
    lst = []
    for i in range(20):
        lst.append(randint(-100, 100))
    theory = list(reversed(sorted(lst)))
    practice = insertion_sort(lst, reverse=True)
    assert theory == list(practice)


def test_input_data_insertion_sort():
    test_lst = [1, 2, 3.41125, '3.14', 'abc']
    with pytest.raises(TypeError):
        list(insertion_sort(test_lst))


def test_stability_ascending_insertion_sort():
    lst = [(1, 1), (2, 5), (3, 2), (4, 6), (5, 5), (6, 10), (7, 8), (8, 5)]
    theory = sorted(lst)
    practice = insertion_sort(lst)
    assert theory == list(practice)


def test_stability_descending_insertion_sort():
    lst = [(1, 1), (2, 5), (3, 2), (4, 6), (5, 5), (6, 10), (7, 8), (8, 5)]
    theory = list(reversed(sorted(lst)))
    practice = insertion_sort(lst, reverse=True)
    assert theory == list(practice)


def test_ascending_quicksort():
    lst = []
    for i in range(20):
        lst.append(randint(-100, 100))
    theory = sorted(lst)
    practice = quicksort(lst)
    assert theory == list(practice)


def test_descending_quicksort():
    lst = []
    for i in range(20):
        lst.append(randint(-100, 100))
    theory = list(reversed(sorted(lst)))
    practice = quicksort(lst, reverse=True)
    assert theory == list(practice)


def test_input_data_quicksort():
    test_lst = [1, 2, 3.41125, '3.14', 'abc']
    with pytest.raises(TypeError):
        list(quicksort(test_lst))


def test_stability_ascending_quicksort():
    lst = [(1, 1), (2, 5), (3, 2), (4, 6), (5, 5), (6, 10), (7, 8), (8, 5)]
    theory = sorted(lst)
    practice = quicksort(lst)
    assert theory == list(practice)


def test_stability_descending_quicksort():
    lst = [(1, 1), (2, 5), (3, 2), (4, 6), (5, 5), (6, 10), (7, 8), (8, 5)]
    theory = list(reversed(sorted(lst)))
    practice = quicksort(lst, reverse=True)
    assert theory == list(practice)
