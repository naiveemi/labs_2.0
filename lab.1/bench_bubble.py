import cProfile
import pstats
from pstats import SortKey
import matplotlib.pyplot as plt
from random import randint
from sort_lib import bubble_sort, shell_sort, insertion_sort, quicksort

def quadratic_complexity(array):
    return [e**2 for e in array]


def correct_lines_c(f):
    result = 1
    lines = [line.strip() for line in f]
    lines_1 = [line for line in lines if line not in '' and line.split()[0].isnumeric() and not line.count('function')]
    for line in lines_1:
        if line.find('') != -1:
            result += int(line.split('   ')[0])
            break
    return result


operation_count = []
operation_lst = []
step = 1000
max_size = 10000
min_size = 1000

for j in range(step, max_size, min_size):
    lst = []
    for x in range(j):
        lst.append(randint(-100, 100))
    operation_count.append(len(lst))
    cProfile.run('bubble_sort(lst)', 'stats.log')
    with open('output.txt', 'w') as log_file_stream:
        p = pstats.Stats('stats.log', stream=log_file_stream)
        p.strip_dirs().sort_stats(SortKey.CALLS).print_stats()
    f = open('output.txt')
    line = correct_lines_c(f)
    f.close()
    operation_lst.append(int(line))
operation_lst_c = [i for i in operation_lst]

array = [i for i in range(min_size, max_size, step)]

plt.style.use('seaborn-whitegrid')
fig, ax = plt.subplots()

ax.set_title('Сложность (пузырьковая сортировка)', fontsize=10, c='indigo')
ax.set_xlabel('Размер массива', fontsize=10, c='indigo')
ax.set_ylabel('Количество операций', fontsize=10, c='indigo')

ax.plot(operation_count, quadratic_complexity(array), lw=2, label='Теоретическая сложность')
ax.plot(operation_count, operation_lst_c, lw=2, c='r', linestyle='--', label='Практическая сложность')

plt.legend()
plt.show()
