def swap():
    return None


def bubble_sort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        swapped = True
        while swapped:
            swapped = False
            for i in range(len(lst) - 1):
                swap()
                if lst[i] > lst[i + 1]:
                    lst[i], lst[i + 1] = lst[i + 1], lst[i]
                    swapped = True
        if not reverse:
            return lst
        elif reverse:
            return list(reversed(lst))


def shell_sort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        cut_res = len(lst) // 2
        while cut_res > 0:
            for value in range(cut_res, len(lst)):
                current_value = lst[value]
                position = value
                swap()
                while position >= cut_res and lst[position - cut_res] > current_value:
                    lst[position] = lst[position - cut_res]
                    position -= cut_res
                    lst[position] = current_value
            cut_res //= 2
        if not reverse:
            return lst
        elif reverse:
            return list(reversed(lst))


def insertion_sort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        for i in range(1, len(lst)):
            key = lst[i]
            j = i - 1
            while j >= 0 and lst[j] > key:
                swap()
                lst[j + 1] = lst[j]
                j -= 1
            lst[j + 1] = key
        if not reverse:
            return lst
        elif reverse:
            return list(reversed(lst))


def quicksort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        swap()
        elem = lst[0]
        left = list(filter(lambda x: x < elem, lst))
        center = list(filter(lambda x: x == elem, lst))
        right = list(filter(lambda x: x > elem, lst))
        if not reverse:
            return quicksort(left) + center + quicksort(right)
        elif reverse:
            return list(reversed(quicksort(left) + center + quicksort(right)))
