from random import randint
import matplotlib.pyplot as plt
from sort_lib import bubble_sort, shell_sort, insertion_sort, quicksort
from timeit import default_timer
import sys

sys.setrecursionlimit(5000)


def spent_time(function, array: list, repeats=1):
    result = 0
    for i in range(repeats):
        start_clock = default_timer()
        function(array)
        end_clock = default_timer() - start_clock

        result += end_clock
    return result/repeats


step = 1000
max_size = 10000
min_size = 1000

names = [[], [], [], []]
names_count = []


for j in range(step, max_size + step, min_size):
    arr = [randint(-100, 100) for x in range(j)]
    names_count.append(len(arr))
    names[0].append(spent_time(bubble_sort, arr))
    names[1].append(spent_time(shell_sort, arr))
    names[2].append(spent_time(insertion_sort, arr))
    names[3].append(spent_time(quicksort, arr))


plt.style.use('seaborn-whitegrid')
fig, ax = plt.subplots(2, 2, figsize=(12, 7))


fig.suptitle('Зависимость времени от размера массива', fontsize=20, c='indigo')


ax[0, 0].set_xlabel('Размер массива', fontsize=10, c='indigo')
ax[0, 1].set_xlabel('Размер массива', fontsize=10, c='indigo')
ax[1, 0].set_xlabel('Размер массива', fontsize=10, c='indigo')
ax[1, 1].set_xlabel('Размер массива', fontsize=10, c='indigo')
ax[0, 0].set_ylabel('Время выполнения', fontsize=10, c='indigo')
ax[0, 1].set_ylabel('Время выполнения', fontsize=10, c='indigo')
ax[1, 0].set_ylabel('Время выполнения', fontsize=10, c='indigo')
ax[1, 1].set_ylabel('Время выполнения', fontsize=10, c='indigo')


ax[0, 0].plot(names_count, names[0], lw=2, c='crimson', label='Пузырьковая сортировка')
ax[0, 1].plot(names_count, names[1], lw=2, c='green', label='Сортировка Шелла')
ax[1, 0].plot(names_count, names[2], lw=2, c='m', label='Сортировка вставками')
ax[1, 1].plot(names_count, names[3], lw=2, c='deepskyblue', label='Быстрая сортировка')
ax[0, 0].legend()
ax[0, 1].legend()
ax[1, 0].legend()
ax[1, 1].legend()


#fig = plt.figure()

#ax_1 = fig.add_subplot(2, 2, 1)
#ax_3 = fig.add_subplot(2, 2, 3)
#ax_4 = fig.add_subplot(2, 2, 4)
#ax_1.plot(names_count, names[0], lw=2, c='crimson', label='Пузырьковая сортировка')
#ax_2.plot(names_count, names[1], lw=2, c='green', label='shell_sort')
#ax_3.plot(names_count, names[2], lw=2, c='m', label='insertion_sort')
#ax_4.plot(names_count, names[3], lw=2, c='deepskyblue', label='quicksort')
#ax_1.legend()
#ax_2.legend()
#ax_3.legend()
#ax_4.legend()


plt.show()
